#Angular - Application Architecture Framework by Mark Webley adobeconsultant.co.uk 

*Angular - EXTRACT FROM MY PATTERN LIBRARY IN GROWING PROGRESS by Mark Webley adobeconsultant.co.uk,* 
*Angular - PATTERN LIBRARY BASE PROJECT SET UP : by Mark Webley adobeconsultant.co.uk*

I used my existing Pattern Library that I created this evening for my personal projects. This code MV* framework is taken from my exiting projects that I am building,
so I can kill many birds with one stone. The answer to some of your questions can be seen at the bottom of this page, but generally an experienced 
and Front End Developer should be able to tell what is going on here, and if they have experience they should have no problems running this project.

All I have to do then in this project is just create a component to satisfy your test. I started your component on 25/04/2016 say about 01:08, yes I aften work into early hours, I build  lot of stuff at the same time whilst between projects and jobs. 


## Link the the end result and be see in the test area of my personal portfolio: 
1.  [one widget](http://www.adobeconsultant.co.uk/tests/grability-test/#/news) | [multiple widgets](http://www.adobeconsultant.co.uk/tests/grability-test/#/newsmultiple)

The architecture is not part of the test, as it is from my Pearl, sweat and tears. 
The widget item is the part of the test, includes, Unit Tests, responsive web, Aria (only partially), Angular, component, directive, controllers, NodeJS API etc,
if I had time I would integrate mongoDB, I can not do more on this, perhaps I will look at it some days later but until then, I have more then 5 other tests to do. 



### Please make sure you have the following tools and dependencies installed on your machine: ###
1.  [NodeJS](http://nodejs.org/) (only used for npm),
2.  [npm](https://www.npmjs.org/),
3.  [ruby](http://rubyinstaller.org/downloads/),
4.  [Sass](http://sass-lang.com/),
5.  [compass](http://compass-style.org/),
6.  [bower](http://bower.io/), (only used for front end tools)
7.  [gulp](http://gulpjs.com/),


### Setting up the project, follow these steps: ###
1. Navigate to the directory of the project via command-line
2. Run command: `$ npm install`
4. Run command: `$ bower update`

### Setting up the project, bower if issues do: ###
1. Navigate to the directory of the project via command-line
2. Run command: `$ npm install bower`
3. Run command: `$ npm install -g bower`
4. Run command: `$ bower update`

### Setting up the project, gulp if issues do: ###
1. Navigate to the directory of the project via command-line
2. Run command: `$ npm install gulp --save-dev`

### To build the SASS file/s, follow these steps: ###
1. Run command: `$ gulp sass:css` 

### DURING DEVELOPMENT RUN THE FOLLOWING COMMANDS: ###)
for continuous integration testing.
1. Run command: `$ gulp`
2. Then all the files will also be in dist/build folder next to the src folder

### TO RUN UNIT TESTS: ###
1. Run command: `$ ./node_modules/karma/bin/karma start` (make sure you have karma installed)

### TO COMPILE AS YOU WORK RUN: ###
1. Run command: `$ gulp watch`


### NOTES FROM THE AUTHOR, MARK WEBLEY: ###
1. I have other application architectures, MVC, MV*, MVP, Pattern Libraries in both AngularJS and BackboneJS,
2. I have years of projects I have worked on in BackboneJS also (with grunt etc), not only AngularJS,
3. I have years of projects I have worked on in core Vanilla JavaScript on the prototype chain,
4. I have even done stuff in DOJO OO JavaScript, dependency injection etc very interesting stuff.


### TODO: -- TO BUILD FOR PRODUCTION RUN THE FOLLOWING COMMANDS: ###
1. Run command: `$ gulp production`,
2. Then all the files will be in dist/build folder next to the src folder



### IN ANSWER TO  YOUR GRABILITY QUESTIONS ###
This project architecture includes what you would normally expect. Services object to all API's, Directives for reusable components,
anotated Javascript for minfication, unit test set up with Continuous Integration and also, builds, optimisation for auto code, minification etc. 


For more information you may contact me on my UK mobile: 0044 07470 139 144, or my email mrkwebley@gmail.com. 