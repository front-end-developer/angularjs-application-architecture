﻿app.config(['$stateProvider', '$sceProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $sceProvider, $urlRouterProvider, $locationProvider) {
    //$locationProvider.hashPrefix('!');
    //$locationProvider.htmlMode({ enabled: true, requireBase: true });

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'js/views/home.html'
        })
        .state('news', {
            url: '/news',
            templateUrl: 'js/views/news.html'
        })
        .state('newsmultiple', {
            url: '/newsmultiple',
            templateUrl: 'js/views/newsmultiple.html'
        })
        .state('page-error-404', {
            url: '/page-errors/404' //,
            //templateUrl: 'partials/page-errors/404.html'
        });
        $urlRouterProvider.otherwise('/');
        // $sceProvider.enabled(false);

    console.log('ui.router loaded');
}]);