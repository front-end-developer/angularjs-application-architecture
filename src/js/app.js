﻿'use strict';
console.log('angular app.js loaded');
var app = angular.module('app.markwebley', [
    'ui.router',
    'ngTouch',
    'ngMessages',
    'ngAnimate',
    'ui.bootstrap',
    'ngSanitize',
    'pascalprecht.translate',
    'angular-carousel'
]);