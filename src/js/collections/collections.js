﻿app.config(['$stateProvider', '$sceProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $sceProvider, $urlRouterProvider, $locationProvider) {
    //$locationProvider.hashPrefix('!');
    //$locationProvider.htmlMode({ enabled: true, requireBase: true });

    $stateProvider
    .state('home', {
        url: '/' //,
        //templateUrl: 'partials/template.html'
    })
    .state('page-error-404', {
        url: '/page-errors/404' //,
        //templateUrl: 'partials/page-errors/404.html'
    });
    $urlRouterProvider.otherwise('/');
    // $sceProvider.enabled(false);

    console.log('ui.router loaded');
}]);