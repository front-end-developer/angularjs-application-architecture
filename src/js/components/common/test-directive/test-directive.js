'use strict';

/**
 * Created by Mark Webley on 24/04/2016.
 */
app.directive('testDirective', [function () {
    return {
        restrict: 'EA',
        scope: {},
        link: function(scope, element, attr) {
            element.addClass('plain');
            element.bind("click", function() {
                scope.clicked = true;
            })
        }
    }
}])