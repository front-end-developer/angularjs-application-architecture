'use strict';

/**
 * Created by Mark Webley on 24/04/2016.
 */
describe("testing news directive", function() {
    var element, scope;
    beforeEach(module("app.markwebley"));
    beforeEach(inject(function ($compile, $rootScope) {
        scope = $rootScope;
        element = angular.element("<div newsDir>{{2 + 2}}</div>");
        $compile(element)($rootScope)
    }))

    it("should equal 4", function() {
        scope.$digest();
        expect(element.html()).toBe("4");
    })

    describe("testDirective", function() {
        it("should add a class of plain", function() {
           expect(element.hasClass("plain")).toBe(true);
        });

        // todo change use protractor instead
        it("should respond to a click event", function() {
            // browserTrigger(element, "click");
            element.triggerHandler('click');
            expect(element.scope().clicked).toBe(true);
        });
    })
})