'use strict';

/**
 * Created by Mark Webley on 24/04/2016.
 */
app.directive('newsDir', [function () {
    return {
        restrict: 'EA',
        templateUrl: 'js/common/news/views/news-tmpl.html',
        scope: {
            news:'='
        },
        link: function(scope, element, attr) {
            console.log('news directive loaded');

            element.addClass('plain');
            element.bind("click", function() {
                console.log('clicked');
                scope.clicked = true;
            })

            scope.expandToggle = function(evt) {
                console.log('hallalujah', evt);
                // debugger;

                var visible = $(evt.currentTarget.nextElementSibling).hasClass('collapse');
                    //evt.currentTarget.nextElementSibling.hasClass('collapse');
                $(evt.currentTarget.nextElementSibling).toggleClass('collapse');
                //if ( visible ) {
                   // $(evt.currentTarget.nextElementSibling).hasClass('collapse')
               // }
            }
        }
    }
}])