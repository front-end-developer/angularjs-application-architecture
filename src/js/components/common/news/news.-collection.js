'use strict';

/**
 * Created by Mark Webley on 24/04/2016.
 * to DO and perfect later
 */
app.directive('newsCollectionDir', [function () {
    return {
        restrict: 'EA',
        require: 'newsDir',
        templateUrl: 'js/common/news/views/news-collection-tmpl.html',
        scope: {
            newslist:'='
        },
        link: function(scope, element, attr, ctrl) {
            console.log('news collection directive loaded');
        }
    }
}])