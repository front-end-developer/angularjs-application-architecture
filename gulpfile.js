/**
 * Created by Mark Webley on 25/03/2016.
 *
 * TODO:    include:
 *          angular annotation like your other projects
 */
var gulp = require('gulp'),
    bower = require('gulp-bower'),
    rimraf =        require('rimraf'),
    concat =        require('gulp-concat'),
    cssmin =        require('gulp-cssmin'),
    csslint =       require('gulp-csslint'),
    autoprefixer =  require('gulp-autoprefixer'),
    htmlmin =       require('gulp-htmlmin'),
    htmlhint =      require('gulp-htmlhint'),
    uglify =        require('gulp-uglify'),
    sass =          require('gulp-sass'),
    config =        require('./gulp.config')();

var paths = {
    webroot: './' + config.webroot + '/'
};

// TODO: check I SHOULD not need this anymore
paths.js = paths.webroot + 'js/**/*.js';
paths.css = paths.webroot + 'css/**/*.css';
paths.minJs = paths.webroot + 'js/**/*.min.js';
paths.cssMin  = paths.webroot + 'css/**/*.min.css';

//DONE
paths.concatJsDest = paths.build + 'js/markwebley.min.js';
paths.concatCssDest = paths.build + 'css/site.min.css';

// TODO: check I SHOULD not need this anymore
paths.buildJS = config.build + 'js/';


/**
 * @description delete js files and folder
 */
gulp.task('clean:js', function (cb) {
    rimraf(paths.concatJsDest, cb);
});

/**
 * @description delete css files and folder
 */
gulp.task('clean:css', function (cb) {
    rimraf(paths.concatCssDest, cb);
});

/**
 * @description Copy project fonts to build
 */
gulp.task('copyProjectFontsToBuild', function () {
    return gulp.src( config.appFonts )
        .pipe(gulp.dest( config.dest.fonts ));
});


/**
 * @description Minify and copy html templates and partials to build
 */
gulp.task('build-html-templates', function () {
    return gulp.src( config.buildHtmlTemplates )
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest( config.dest.build ));
});

/**
 * @description Minify & copy html templates for partials to build...views
 */
gulp.task('build-html-partial-views', function () {
    return gulp.src( config.buildHtmlPartialViews )
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest( config.dest.views ));
});

/**
 * @description Minify & copy html templates for component views to build...component
 */
gulp.task('build-html-component-views', function () {
    return gulp.src( config.buildHtmlComponentViews )
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest( config.dest.js ));
});

/**
 * @description HTML hint, partials, html, component views
 */
gulp.task('htmlhint', function () {
    gulp.src( config.htmlhint )
        .pipe(htmlhint({
            'htmlhintrc': './.htmlhintrc'
        }))
        .pipe(htmlhint.reporter());
});

/**
 * @description Minify JS
 */
gulp.task('min:js', function () {
    console.log('running min:js');
    return gulp.src( config.js )
        .pipe(concat('markwebley.min.js'))
        .pipe(uglify({
            outSourceMap: true,
            expand: true,
            cwd: '.',
            beautify: true,
            mangle: false,
            force: true,
            ignores: true,
            comments: true,
            compress: {
                drop_debugger: false
            }
        }).on('error', function (err) {
            console.log('min:js error', err);
        }))
        .pipe(gulp.dest( config.dest.js )); // 'build/js'
});

/**
 * @description Autoprefix CSS
 */
gulp.task('autoprefix:css', function () {
    return gulp.src( config.autoprefix )
        .pipe(autoprefixer( config.autoprefixer ))
        .pipe(gulp.dest( config.dest.srcCss ));
});

/**
 * @description CSS linting
 *              & csslint:    http://csslint.net/about.html
 *              & consider:     https://2002-2012.mattwilcox.net/archive/entry/id/1054/
 */
gulp.task('csslint:css', function () {
    return gulp.src( config.csslint )
        .pipe(csslint( config.csslintSettings ))    // TODO use external file instead
        .pipe(csslint.reporter());
});

/**
 * @description Minify CSS
 */
gulp.task('min:css', ['sass:css', 'autoprefix:css'], function () {
    gulp.src( config.cssmin )
        .pipe(concat('markwebley.min.css'))
        .pipe(cssmin())
        .pipe(gulp.dest( config.dest.css ));
});

/**
 * @description Do SASS preprocessing
 */
gulp.task('sass:css', function () {
    gulp.src( config.sassCSS )
        .pipe(sass().on('error', sass.logError))
        .pipe(sass())
        .pipe(gulp.dest( config.dest.srcCss ));
});

/**
 * @description Watch file changes and apply processes
 */
gulp.task('watch', function () {
    //gulp.watch( 'gulp.config.js' );
    //gulp.watch( 'gulpfile.js' );
    gulp.watch( config.watch.css , ['sass:css'] );
    gulp.watch( config.watch.js, ['min:js'] );
    gulp.watch( config.watch.htmlComponents, ['build-html-component-views'] );

    gulp.watch( config.watch.html, [
        'htmlhint',
        'build-html-partial-views',
        'build-html-templates'
    ] );
});

gulp.task('clean', [
    'clean:js',
    'clean:css'
]);

gulp.task('min', [
    'min:js',
    'min:css'
]);

gulp.task('default', [
    'clean',
    'sass:css',
    'min',
    'build-html-partial-views',
    'build-html-templates',
    'build-html-component-views',
    'copyProjectFontsToBuild',
    'watch'
], function() {
    console.log('building on default set up');
});


// TODO: why do I still need this....
gulp.task('bower', function() {
    return bower();
})
