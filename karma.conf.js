module.exports = function(config){
  config.set({

    basePath : '',

    files : [
        'src/js/libs/bower_components/jquery/dist/jquery.min.js',
        'src/js/libs/bootstrap-3.3.6-dist/js/bootstrap.min.js',
        'src/js/libs/bower_components/angular/angular.min.js',
        'src/js/libs/bower_components/angular-mocks/angular-mocks.js',
        'src/js/libs/bower_components/angular-ui-router/release/angular-ui-router.min.js',
        'src/js/libs/bower_components/angular-carousel/dist/angular-carousel.js',
        'src/js/libs/bower_components/angular-touch/angular-touch.min.js',
        'src/js/libs/bower_components/angular-loader/angular-loader.min.js',
        'src/js/libs/bower_components/angular-ui-router/release/angular-ui-router.min.js',
        'src/js/libs/bower_components/angular-messages/angular-messages.min.js',
        'src/js/libs/bower_components/angular-sanitize/angular-sanitize.min.js',
        'src/js/libs/bower_components/angular-translate/angular-translate.min.js',
        'src/js/libs/bower_components/angular-animate/angular-animate.min.js',
        'src/js/libs/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'src/js/libs/bower_components/angular-bootstrap/ui-bootstrap.min.js',
        // 'src/js/libs/bower_components/angular-scenario/angular-scenario.js',
        //'src/js/libs/bower_components/protractor/lib/protractor.js',
        'src/js/libs/bower_components/ngToast/dist/ngToast.min.js',
        'src/js/app.js',
        'src/js/components/**/*.js',
     //  'src/js/components/**/*.spec.js',
      'src/js/views/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    colors: true,

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
