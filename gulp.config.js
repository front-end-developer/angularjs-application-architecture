/**
 *  @author     Mark Webley @adobeconsultant.co.uk
 */
module.exports = function () {
    var jsSrc = 'src/js/',
        cssSrc = 'src/resources/';


    var config = {

        /**
         * @description     application root
         *                  could be src or depending on the project 'app' or like 'app/src'
         */
        webroot: 'src',

        /**
         * @description     build directory file project
         */
        build: [
            'build/'
        ],

        /**
         * @description     temp directory file paths
         *
         *                  TODO:   I may not need this
         */
        temp: 'temp/',

        /**
         * @description     local directory file css paths
         */
        buildCss: 'src/css/',

        /**
         * @description     Javascript file paths
         **/
        alljs: [
            jsSrc + 'routes/*.js',
            jsSrc + 'components/**/*.js',
            jsSrc + 'models/**/*.js',
            jsSrc + 'views/**/*.js',
            jsSrc + 'collections/**/*.js',
            jsSrc + 'services/**/*.js',
            jsSrc + 'controllers/**/*.js',
            jsSrc + 'app.js'
        ],

        jsLib: jsSrc + 'lib/**/*.js',

        index: 'src/index.html',

        /**
         * @description     specific Javascript file paths to compile
         *                  used by gulp task:
         *                  min:js
         *
         *                   // TODO: test and combine: components/common/, components/desktop/, components/mobile/
         *                      may need this:  '!' + jsSrc + 'lib/',
         *
         *                      removed but test:
         **/
        js: [
            // libraries
            jsSrc + 'libs/bower_components/jquery/dist/jquery.js',
            jsSrc + 'libs/bootstrap-3.3.6-dist/js/bootstrap.js',
            jsSrc + 'libs/bower_components/angular/angular.js',
            //jsSrc + 'libs/bower_components/angular-mocks/angular-mocks.js',
            jsSrc + 'libs/bower_components/angular-ui-router/release/angular-ui-router.js',
            jsSrc + 'libs/bower_components/angular-carousel/dist/angular-carousel.js',
            jsSrc + 'libs/bower_components/angular-touch/angular-touch.js',
            jsSrc + 'libs/bower_components/angular-loader/angular-loader.js',
            jsSrc + 'libs/bower_components/angular-messages/angular-messages.js',
            jsSrc + 'libs/bower_components/angular-sanitize/angular-sanitize.js',
            jsSrc + 'libs/bower_components/angular-translate/angular-translate.js',
            jsSrc + 'libs/bower_components/angular-animate/angular-animate.js',
            jsSrc + 'libs/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            jsSrc + 'libs/bower_components/angular-bootstrap/ui-bootstrap.js',
            jsSrc + 'libs/bower_components/ngToast/dist/ngToast.js',

            // likely not neccesary BUT kept for later updates
            '!' + jsSrc + '**/*.min.js',
            '!' + jsSrc + '**/*.spec.js',

            // source
            jsSrc + 'app.js',

            // quick hack
            jsSrc + 'components/common/**/news.js',
            jsSrc + 'components/common/**/news-collection.js',

            '!' + jsSrc + 'components/**/*.spec.js',
            jsSrc + 'components/common/**/*.js',
            jsSrc + 'components/desktop/**/*.js',
            jsSrc + 'components/mobile/**/*.js',
            jsSrc + 'components/**/*.js',
            jsSrc + 'models/**/*.js',
            jsSrc + 'routes/**/*.js',
            jsSrc + 'controllers/**/*.js',
            jsSrc + 'collections/**/*.js',
            jsSrc + 'services/**/*.js',
            jsSrc + 'utilities/**/*.js',
            jsSrc + 'views/**/*.js'


        ],

        autoprefix: [
            'src/css/style.css'
        ],

        csslint: [
            'src/css/*.css'
        ],

        cssmin: [
            'src/js/libs/bootstrap-3.3.6-dist/css/bootstrap.css',
            'src/css/reset/*.css',
            'src/css/animate/*.css',
            'src/css/*.css',
            '!src/css/**/*.min.css'
        ],

        /**
         * @description     css helpers and libraries to copy
         **/
        cssCopy: [
            'src/css/animate/animate.css',
            'src/css/reset/normalize.css'
        ],

        /**
         * @description     css pre-processor file paths
         **/
        sass: [
            jsSrc + 'components/common/**/sass/*.scss',
            jsSrc + 'components/desktop/**/sass/*.scss',
            jsSrc + 'components/mobile/**/sass/*.scss',
            cssSrc + 'sass/style.scss'
        ],

        /**
         * @description     Bower and NPM locations
         **/
        bower: {
            json: require('./bower.json'),
            directory: jsSrc + 'libs/bower_components',
            ignorePath: '../'
        },

        /**
         * @description     location for tests
         */
        specs: [
            jsSrc + 'components/**/test/spec/*-[Ss]pec.js',
            'tests/spec/*-[Ss]pec.js'
        ],

        /**
         * @description     css browser support
         */
        autoprefixer: {
            browsers: [
                'last 3 Chrome versions',
                'last 3 Firefox versions',
                'last 3 Android versions',
                'ios >= 7',
                'ie >= 9'
            ],
            cascade: false
        },

        /**
         * IN USE:
         *
         * @description     css lint
         */
        csslintSettings: {
            'box-model': true,
            'display-property-grouping': true,
            'duplicate-properties': true,
            'empty-rules': true,
            'known-properties': true,
            'non-link-hover': false,
            'adjoining-classes': false,
            'box-sizing': false,
            'compatible-vendor-prefixes': true,
            'gradients': true,
            'text-indent': true,
            'vendor-prefix': true,
            'fallback-colors': true,
            'star-property-hack': true,
            'underscore-property-hack': true,
            'bulletproof-font-face': false,
            'font-faces': true,
            'import': true,
            'regex-selectors': true,
            'universal-selector': true,
            'unqualified-attributes': true,
            'zero-units': true,
            'overqualified-elements': true,
            'shorthand': true,
            'duplicate-background-images': true,
            'floats': true,
            'font-sizes': true,
            'ids': true,
            'important': false,
            'outline-none': true,
            'qualified-headings': true,
            'unique-headings': true,
            'errors': true,
            'rules-count': true,
            'selector-max-approaching': true,
            'selector-max': true
        },

        /**
         * IN USE:
         *
         * @description     partial files
         */
        buildHtmlPartialViews: [
            jsSrc + 'views/**/*.html',
        ],

        /**
         * IN USE:
         *
         * @description     partial files
         */
        buildHtmlComponentViews: [
            jsSrc + 'components/**/views/*.html'
        ],




        /**
         * @description     all fonts that you want ot reference or to copy from
         */
        appFonts: [
            'src/js/libs/bootstrap-3.3.6-dist/fonts/glyphicons-halflings-regular.eot',
            'src/js/libs/bootstrap-3.3.6-dist/fonts/glyphicons-halflings-regular.svg',
            'src/js/libs/bootstrap-3.3.6-dist/fonts/glyphicons-halflings-regular.ttf',
            'src/js/libs/bootstrap-3.3.6-dist/fonts/glyphicons-halflings-regular.woff',
            'src/js/libs/bootstrap-3.3.6-dist/fonts/glyphicons-halflings-regular.woff2'
        ],


        /**
         * @description     root template and partial files
         */
        buildHtmlTemplates: [
            'src/*.html'
        ],

        /**
         * @description     root template and partial files
         */
        htmlhint: [
            'src/*.html',
            'src/js/components/**/views/*.html',
            'src/js/views/**/*.html'
        ],

        /**
         * @description     root template and partial files
         */
        sassCSS: [
            './src/resources/sass/**/style.scss'
        ],

        /**
         * @description     watch for changes and build
         *                  used by gulp:watch
         */
        watch: {
            css:            'src/resources/sass/**/*.scss',
            cssComponents:  'src/js/components/**/*.scss',
            js:             'src/js/**/*.js',    // TODO: to check - if this does not watch components add it below this line
            libs:           '!src/js/libs',     // TODO: check that this works
            htmlComponents: 'src/js/components/**/*.html',
            html:           'src/*.html',
            partials:       'src/js/views/**/*.html',
        },

        /**
         * IN USE:
         *
         * @description     watch for changes and build
         */
        dest: {
            build:          'build/',
            views:          'build/js/views/',
            js:             'build/js',
            css:            './build/css',
            srcCss:         'src/css/',
            fonts:          'build/fonts/'
        }
    }

    return config;
}